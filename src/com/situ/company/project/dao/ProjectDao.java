package com.situ.company.project.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.situ.company.project.model.ProjectModel;
import com.situ.company.util.JDBCUtil;

public class ProjectDao {
	private static String table = "project";
	private static String cols = "code,name,time";


	
	public Integer insert(ProjectModel model) {
		StringBuffer sql = new StringBuffer("insert into ");
		sql.append(table).append('(').append(cols).append(")values(?,?,?)");
		
		return JDBCUtil.update(sql.toString(), model.getCode(),model.getName(),model.getTime());
	}
	
	public List<ProjectModel> select(ProjectModel model) {
		StringBuffer sql = new StringBuffer("select ");
		sql.append(cols).append(" from ").append(table);
		List<Object> vals = appendWhere(sql,model);
		Map<String,String> map = new HashMap<>();
		map.put("code", "code");
		map.put("name", "name");
		map.put("time", "time");
		return JDBCUtil.query(sql.toString(), vals,ProjectModel.class , map);
	}
	
	public Integer delete(ProjectModel model) {
		StringBuffer sql = new StringBuffer("delete from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		return JDBCUtil.update(sql.toString(), vals);
	}
	
	public Integer update(ProjectModel model) {
		StringBuffer sql = new StringBuffer("update ");
		sql.append(table);
		List<Object> vals = appendSet(sql,model);
		return JDBCUtil.update(sql.toString(), vals);
	}
	private List<Object> appendSet(StringBuffer sql,ProjectModel model){
		sql.append(" set id=id");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null ) {
			sql.append(" ,code = ? ");
			objs.add(code);
		}
		String name = model.getName();
		if(name != null ) {
			sql.append(" ,name = ? ");
			objs.add(name);
		}
		String time = model.getTime();
		if(time != null ) {
			sql.append(" ,time = ? ");
			objs.add(time);
		}
		sql.append(" where code = ?");
		objs.add(code);
		return objs;
	}
	private List<Object> appendWhere(StringBuffer sql,ProjectModel model) {
		sql.append(" where 1=1 ");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null && !"".equals(code.trim())) {
			sql.append(" and code like ? ");
			objs.add(code);
		}
		String name = model.getName();
		if(name != null && !"".equals(name.trim())) {
			sql.append(" and name like ? ");
			objs.add(name);
		}
		String time = model.getTime();
		if(time != null && !"".equals(time.trim())) {
			sql.append(" and time like ? ");
			objs.add(time);
		}
		String order = model.getOrderby();
		if(order != null && !"".equals(order.trim())) {
			sql.append(" order by "+order);
		}
		if(model.isPageOn()) {
			sql.append(" limit "+model.getRowStart()+","+model.getPageLimit());
		}
		
		
		return objs;
		
	}
	/**
	 * 分页查总数
	 * 
	 * @param mdb
	 * @return
	 */
	public int selectCount(ProjectModel mdb) {
		StringBuffer sql = new StringBuffer("select count(id) from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,mdb);
		
		return JDBCUtil.queryInt(sql.toString(), vals);
	}
}
