package com.situ.company.project.model;

import com.situ.company.util.PagerModel;

public class ProjectModel extends PagerModel{
	private String code;
	private String name;
	private String time;
	
	
	
	
	public ProjectModel() {
		super();
	}
	public ProjectModel(String code, String name, String time) {
		super();
		this.code = code;
		this.name = name;
		this.time = time;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	
	
}
