package com.situ.company.project.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.situ.company.project.model.ProjectModel;
import com.situ.company.project.service.ProjectService;
import com.situ.company.util.FmtRequest;
@WebServlet("/projectServlet")
public class ProjectServlet extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	ProjectService service = new ProjectService();
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		String result = "";
		switch(action) {
		case "list":
			result = list(req);
			break;
		case "add":
			result = add(req);
			break;
		case "del":
			result = del(req);
			break;
		case "get":
			result = get(req);
			break;
		case "upd":
			result = upd(req);
			break;
		case "sel":
			result = sel(req);
			break;
			
		}
		FmtRequest.out(resp, result);
	}
	
	
	
	
	
	private String sel(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		return new JSONArray(service.selectList(model)).toString();
	}





	private String upd(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		return service.update(model);
	}





	private String get(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		
		return new JSONObject(service.selectModel(model)).toString();
	}





	private String del(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		
		return service.delete(model);
	}





	private String add(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		
		return service.insert(model);
	}





	private String list(HttpServletRequest req) {
		ProjectModel model = FmtRequest.parseModel(req, ProjectModel.class);
		int count = service.selectCount(model);
		model.setPageOn(true);
		//这里是因为parseModel方法的反射用到的getdeclare...方法不能获得从父类继承的属性
		model.setPageIndex(Integer.valueOf(req.getParameter("pageIndex")));
		model.setPageLimit(Integer.valueOf(req.getParameter("pageLimit")));
		List<ProjectModel> list =  service.selectList(model);
		JSONObject json = new JSONObject();
		json.put("count", count);
		json.put("data", list);
		                                                        
		return json.toString();
	}





	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
}
