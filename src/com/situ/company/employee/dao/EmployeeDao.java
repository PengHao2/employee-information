package com.situ.company.employee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.util.JDBCUtil;

public class EmployeeDao {
	private static String table = "employee";
	private static String cols = "code,password,name,gender,entry_time,department_code,image";
	private Connection conn;
	private PreparedStatement ps;

	public Integer insert(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("insert into ");
		sql.append(table).append('(').append(cols).append(")values(?,?,?,?,?,?,?)");
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, model.getCode());
			ps.setString(2, model.getPassword());
			ps.setString(3, model.getName());
			ps.setString(4, model.getGender());
			ps.setString(5, model.getEntryTime());
			ps.setString(6, model.getDepartmentCode());
			ps.setString(7, model.getImage());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	
	public List<EmployeeModel> select(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("select ");
		sql.append(cols).append(",(select name from department where code=employee.department_code )department_name")
		.append(" from ").append(table);
		List<Object> vals = appendWhere(sql,model);
		ResultSet rs =null;
		List<EmployeeModel> list = new ArrayList<>();
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				EmployeeModel mdb = new EmployeeModel();
				mdb.setCode(rs.getString("code"));
				mdb.setPassword(rs.getString("password"));
				mdb.setName(rs.getString("name"));
				mdb.setGender(rs.getString("gender"));
				mdb.setEntryTime(rs.getString("entry_time"));
				mdb.setDepartmentCode(rs.getString("department_code"));
				mdb.setImage(rs.getString("image"));
				mdb.setDepartmentName(rs.getString("department_name"));
				
				
				list.add(mdb);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps, rs);
		}
		return list;
	}
	
	public Integer delete(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("delete from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	
	public Integer update(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("update ");
		sql.append(table);
		List<Object> vals = appendSet(sql,model);
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	/**
	 * 分页查询分页控件的count参数
	 * 
	 * @param model
	 * @return
	 */
	public int selectCount(EmployeeModel model) {
		StringBuffer sql = new StringBuffer("select count(id) from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		ResultSet rs =null;
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps, rs);
		}
		return 0;
	}
	private List<Object> appendSet(StringBuffer sql,EmployeeModel model){
		sql.append(" set id=id");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null ) {
			sql.append(" ,code = ? ");
			objs.add(code);
		}
		String password = model.getPassword();
		if(password != null ) {
			sql.append(" ,password = ? ");
			objs.add(password);
		}
		String name = model.getName();
		if(name != null ) {
			sql.append(" ,name = ? ");
			objs.add(name);
		}
		String gender = model.getGender();
		if(gender != null ) {
			sql.append(" ,gender = ? ");
			objs.add(gender);
		}
		String entryTime = model.getEntryTime();
		if(entryTime != null ) {
			sql.append(" ,entry_time = ? ");
			objs.add(entryTime);
		}
		String departmentCode = model.getDepartmentCode();
		if(departmentCode != null ) {
			sql.append(" ,department_code = ? ");
			objs.add(departmentCode);
		}
		String image = model.getImage();
		if(image != null ) {
			sql.append(" ,image = ? ");
			objs.add(image);
		}
		
		sql.append(" where code = ?");
		objs.add(code);
		return objs;
	}
	private List<Object> appendWhere(StringBuffer sql,EmployeeModel model) {
		sql.append(" where 1=1 ");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null && !"".equals(code.trim())) {
			sql.append(" and code like ? ");
			objs.add(code);
		}
		String password = model.getPassword();
		if(password != null && !"".equals(password.trim())) {
			sql.append(" and password like ? ");
			objs.add(password);
		}
		String name = model.getName();
		if(name != null && !"".equals(name.trim())) {
			sql.append(" and name like ? ");
			objs.add(name);
		}
		String gender = model.getGender();
		if(gender != null && !"".equals(gender.trim())) {
			sql.append(" and gender like ? ");
			objs.add(gender);
		}
		String entryTime = model.getEntryTime();
		if(entryTime != null && !"".equals(entryTime.trim())) {
			sql.append(" and entry_time like ? ");
			objs.add(entryTime);
		}
		String departmentCode = model.getDepartmentCode();
		if(departmentCode != null && !"".equals(departmentCode.trim())) {
			sql.append(" and department_code like ? ");
			objs.add(departmentCode);
		}
		String order = model.getOrderby();
		if(order != null && !"".equals(order.trim())) {
			sql.append(" order by "+order);
		}
		if(model.isPageOn()) {
			sql.append(" limit "+model.getRowStart()+","+model.getPageLimit());
		}
		
		return objs;
		
	}

}
