package com.situ.company.employee.servlet;


import java.io.IOException;
import java.util.List;
import java.util.Map;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.fileupload.FileUploadException;
import org.json.JSONObject;

import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.employee.service.EmployeeService;
import com.situ.company.util.FmtRequest;
import com.situ.company.util.UploadUtil;
@WebServlet("/employeeUploadServlet")
public class EmployeeUploadServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private EmployeeService service = new EmployeeService();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, Object> map = null;
		try {
			map = UploadUtil.upload(req);
		} catch (FileUploadException | IOException e) {
			e.printStackTrace();
		}
		
		String code = map.get("code").toString();
		EmployeeModel model = new EmployeeModel();
		//先查原头像并删掉
		EmployeeModel mdb = service.selectModel(model);
		String image = mdb.getImage();
		if(image != null && !image.trim().isEmpty()) {
			UploadUtil.delFile(image);
		}
		
		model.setCode(code);
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>)map.get("list");
		image = list.get(0);
		model.setImage(image);
		
		String res = service.update(model);
		
		JSONObject result = new JSONObject();
		result.put("code", res);
		result.put("image", image);
		FmtRequest.out(resp,result.toString());
	}
	
}
