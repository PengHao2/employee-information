package com.situ.company.employee.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.situ.company.employee.model.EmployeeModel;
import com.situ.company.employee.service.EmployeeService;
import com.situ.company.util.FmtEmpty;
import com.situ.company.util.FmtRequest;
import com.situ.company.util.MD5;
import com.situ.company.util.UploadUtil;
@WebServlet("/employeeServlet")
public class EmployeeServlet extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	// 控制层:  接受请求 获取参数 封装对象 调用方法(传递数据) 返回结果(页面跳转)
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String result = null;
		String action = req.getParameter("action");
		switch(action) {
			case "reg":
			case "add":
				result = reg(req);
				break;
			case "login":
				result = login(req);
				break;
			case "logout":
				req.getSession().removeAttribute("user");
				resp.sendRedirect(req.getContextPath()+"/web/login.jsp");
				break;
			case "sel":
				result = sel(req);
				break;
			case "list":
				result = list(req);
				break;
			case "del":
				result = del(req);
				break;
			case "upd":
				result = upd(req);
				break;
			case "get":
				result = get(req);
				break;
			case "resetPass":
				result = resetPass(req);
				break;
			case "updPass":
				result = updPass(req);
				break;
			case "updDepartment":
				result = updDepartment(req);
				break;
			case "delPic":
				result = delPic(req);
				break;
			default:
				break;
					
		}
		
		FmtRequest.out(resp,result);
		
		
	}
	private String delPic(HttpServletRequest req) {
		String code = req.getParameter("code");
		EmployeeModel model = new EmployeeModel(code,null,null);
		EmployeeModel dbm = service.selectModel(model);
		String image = dbm.getImage();
		UploadUtil.delFile(image);
		model.setImage("");
		return service.update(model);
	}
	/**
	 * 修改部门
	 * 
	 * @param req
	 * @return
	 */
	private String updDepartment(HttpServletRequest req) {
		String code = req.getParameter("code");
		String departmentCode = req.getParameter("departmentCode");
		EmployeeModel model = new EmployeeModel(code,null,null,null,null,departmentCode);
		return service.update(model);
		
	}
	/**
	 * 修改密码
	 *  
	 * @param req
	 * @return
	 */
	private String updPass(HttpServletRequest req) {
		String code = req.getParameter("code");
		String pass = req.getParameter("pass");
		if(FmtEmpty.isEmpty(pass)) {
			pass = getDefaultPass();
		}else {
			pass = MD5.encode(pass);
		}
		EmployeeModel model = new EmployeeModel(code,pass, null);
		return service.update(model);
	}
	/**
	 * 密码重置
	 * 
	 * @param req
	 * @return
	 */
	private String resetPass(HttpServletRequest req) {
		String code = req.getParameter("code");
		EmployeeModel model = new EmployeeModel(code, getDefaultPass(), null);
		return service.update(model);
		
	}
	/**
	 * 按编号查询一个员工,数据回显
	 * @param req
	 * @return
	 */
	private String get(HttpServletRequest req) {
		String code = req.getParameter("code");
		EmployeeModel model = new EmployeeModel(code, null, null);


		return new JSONObject(service.selectModel(model)).toString();
	}
	/**
	 * update员工信息
	 * @param req
	 * @return
	 */
	private String upd(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String gender = req.getParameter("gender");
		
		String entryTime = req.getParameter("entryTime");
		EmployeeModel model = new EmployeeModel(code,null,name,gender,entryTime,null);
		
		
		return service.update(model);
	}


	private EmployeeService service = new EmployeeService();
	/**
	 * 删除员工
	 * @param req
	 * @return
	 */
	private String del(HttpServletRequest req) {
		String code = req.getParameter("code");
		EmployeeModel model = new EmployeeModel(code, null, null);
		return service.delete(model);

	}
	/**
	 * 分页
	 * 
	 * @param req
	 * @return
	 */
	private String list(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String pageIndex = req.getParameter("pageIndex");
		String pageLimit = req.getParameter("pageLimit");
		EmployeeModel model = new EmployeeModel(code, null, name);
		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);
		
		Map<String,Object> map = new HashMap<>();
		  
		map.put("data", service.selectList(model)); 
		map.put("count",service.selectCount(model)); 
		return new JSONObject(map).toString();
		
	}
	private String sel(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		EmployeeModel model = new EmployeeModel(code, null, name);

		List<EmployeeModel> list = service.selectList(model);

		return new JSONArray(list).toString();

	}
	private String reg(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String pass = req.getParameter("pass");
		String gender = req.getParameter("gender");
		
		String entryTime = req.getParameter("entryTime");
		//没输入密码设置初始密码
		if(FmtEmpty.isEmpty(pass)) {
			pass = getDefaultPass();
		}else {
			pass = MD5.encode(pass);
		}
		if(req.getParameter("action")=="reg") {
			String authCode = req.getParameter("authCode");
			Object authCodeSession = req.getSession().getAttribute("authCodeSession");
			if(!authCodeSession.toString().equals(authCode)) {
				return "A";
			}
			
		}
		//System.out.println(code+"\t"+name+"\t"+pass);
		//System.out.println(authCode);
		EmployeeModel model = new EmployeeModel(code,pass,name,gender,entryTime,null);
		
		String result = service.insert(model);
		return result;
		
	}
	/**
	 * 账户登录,返回0=账号不存在,返回1=登录成功,返回2=密码错误,返回A验证码错误
	 * @param req
	 * @return
	 */
	private String login(HttpServletRequest req) {
		String code = req.getParameter("code");
		String pass = req.getParameter("pass");
		String authCode = req.getParameter("authCode");
		Object authCodeSession = req.getSession().getAttribute("authCodeSession");
		if(!authCodeSession.toString().equals(authCode)) {
			return "A";
		}
		EmployeeModel model = new EmployeeModel(code,pass,null);
		
		EmployeeModel mdb = service.selectModel(model);
		if(mdb == null)
			return "0";
		pass = MD5.encode(pass);
		if(!mdb.getPassword().equals(pass))
			return "2";
		req.getSession().setAttribute("user", mdb);
		return "1";
		//System.out.println(model.getPassword());
		//System.out.println(mdb.getPassword());
		//return mdb.getPassword().equals(model.getPassword())?"1":"2";
	}
	private String getDefaultPass(){
		return MD5.encode("123456");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
