package com.situ.company.employee.model;

import com.situ.company.util.PagerModel;

public class EmployeeModel extends PagerModel{
	private String code;
	private String password;
	private String name;
	private String gender;
	private String entryTime;
	private String departmentCode;
	private String image;
	
	private String departmentName;
	
	
	
	
	public EmployeeModel() {
		super();
	}

	public EmployeeModel(String code, String password, String name) {
		super();
		this.code = code;
		this.password = password;
		this.name = name;
	}
	
	
	
	public EmployeeModel(String code, String password, String name, String gender, String entryTime,
			String departmentCode, String image) {
		super();
		this.code = code;
		this.password = password;
		this.name = name;
		this.gender = gender;
		this.entryTime = entryTime;
		this.departmentCode = departmentCode;
		this.image = image;
	}

	public EmployeeModel(String code, String password, String name, String gender, String entryTime,
			String departmentCode) {
		super();
		this.code = code;
		this.password = password;
		this.name = name;
		this.gender = gender;
		this.entryTime = entryTime;
		this.departmentCode = departmentCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(String entryTime) {
		this.entryTime = entryTime;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

}
