package com.situ.company.employee.service;

import java.util.List;


import com.situ.company.employee.dao.EmployeeDao;
import com.situ.company.employee.model.EmployeeModel;

public class EmployeeService {

	EmployeeDao dao = new EmployeeDao();

	public String insert(EmployeeModel model) {
		// 添加的逻辑主键不存在时允许添加,否则不允许添加
		EmployeeModel mdb = selectModel(model);
		if(mdb != null) {
			return "E";
		}
		Integer result = dao.insert(model);
		return String.valueOf(result);
	}

	public String delete(EmployeeModel model) {
		Integer result = dao.delete(model);
		return String.valueOf(result);
	}

	public String update(EmployeeModel model) {
		Integer result = dao.update(model);
		return String.valueOf(result);
	}
	/**
	 * 多条件的模糊查询出一些记录
	 * 
	 * @param model
	 * @return List<EmployeeModel>
	 */
	public List<EmployeeModel> selectList(EmployeeModel model) {
		String code = model.getCode();
		if (code != null)
			model.setCode("%" + code + "%");
		String name = model.getName();
		if (name != null)
			model.setName("%" + name + "%");
		return dao.select(model);
	}
	/**
	 * 根据(逻辑)主键查询出唯一记录,主键不存在则返回null
	 * 
	 * @param model
	 * @return EmployeeModel
	 */
	public EmployeeModel selectModel(EmployeeModel model) {
		EmployeeModel temp = new EmployeeModel();
		temp.setCode(model.getCode());
		List<EmployeeModel> list = dao.select(temp);
		if(list == null || list.isEmpty()) 
			return null;
		return list.get(0);
	}
	/**
	 * 根据多条件的模糊查询出一些记录的条数count
	 * 
	 * @param model
	 * @return
	 */
	public int selectCount(EmployeeModel model) {
		EmployeeModel mdb = new EmployeeModel();
		String code = model.getCode();
		mdb.setCode(code == null?"%%" : "%" + code + "%");
		String name = model.getName();
		mdb.setName(name == null?"%%" : "%" + name + "%");
		return dao.selectCount(mdb);
	}

}
