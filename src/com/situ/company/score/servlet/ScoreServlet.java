package com.situ.company.score.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.situ.company.score.model.ScoreModel;
import com.situ.company.score.service.ScoreService;
import com.situ.company.util.FmtRequest;
@WebServlet("/scoreServlet")
public class ScoreServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ScoreService service= new ScoreService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
 	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		String result ="";
		switch(action) {
		case "list":
			result = list(req);
			break;
		case "add":
			result = add(req);
			break;
		case "del":
			result = del(req);
			break;
		case "upd":
			result = upd(req);
			break;
		case "get":
			result = get(req);
			break;
		}
		FmtRequest.out(resp, result);
	}
 	private String get(HttpServletRequest req) {
 		ScoreModel model = FmtRequest.parseModel(req, ScoreModel.class);
		return new JSONObject(service.selectModel(model)).toString();
	}
	private String upd(HttpServletRequest req) {
 		ScoreModel model = FmtRequest.parseModel(req, ScoreModel.class);
		return service.update(model);
	}
	private String del(HttpServletRequest req) {
 		ScoreModel model = FmtRequest.parseModel(req, ScoreModel.class);
		return service.delete(model);
	}
	private String add(HttpServletRequest req) {
 		ScoreModel model = FmtRequest.parseModel(req, ScoreModel.class);
		return service.insert(model);
	}
	/**
 	 * 分页显示
 	 * 
 	 * @param req
 	 * @return
 	 */
	private String list(HttpServletRequest req) {
		ScoreModel model = FmtRequest.parseModel(req, ScoreModel.class);
		int count = service.selectCount(model);
		model.setPageOn(true);
		model.setPageIndex(Integer.valueOf(req.getParameter("pageIndex")));
		model.setPageLimit(Integer.valueOf(req.getParameter("pageLimit")));
		List<ScoreModel> list =  service.selectList(model);
		JSONObject json = new JSONObject();
		json.put("count", count);
		json.put("data", list);
		                                                        
		return json.toString();
	}
	

}
