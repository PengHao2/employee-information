package com.situ.company.score.model;

import com.situ.company.util.PagerModel;

public class ScoreModel extends PagerModel{
	private String employeeCode;
	private String projectCode;
	private String score;
	
	
	
	public ScoreModel() {
		super();
	}
	public ScoreModel(String employeeCode, String projectCode, String score) {
		super();
		this.employeeCode = employeeCode;
		this.projectCode = projectCode;
		this.score = score;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	
	
}
