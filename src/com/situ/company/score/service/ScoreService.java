package com.situ.company.score.service;

import java.util.List;


import com.situ.company.score.dao.ScoreDao;
import com.situ.company.score.model.ScoreModel;

public class ScoreService {
	ScoreDao dao = new ScoreDao();

	public String insert(ScoreModel model) {
		// 添加的逻辑主键不存在时允许添加,否则不允许添加
		ScoreModel mdb = selectModel(model);
		if(mdb != null) {
			return "E";
		}
		Integer result = dao.insert(model);
		return String.valueOf(result);
	}

	public String delete(ScoreModel model) {
		Integer result = dao.delete(model);
		return String.valueOf(result);
	}

	public String update(ScoreModel model) {
		Integer result = dao.update(model);
		return String.valueOf(result);
	}
	/**
	 * 多条件的模糊查询出一些记录
	 * 
	 * @param model
	 * @return List<EmployeeModel>
	 */
	public List<ScoreModel> selectList(ScoreModel model) {
		String employeeCode = model.getEmployeeCode();
		if (employeeCode != null)
			model.setEmployeeCode("%" + employeeCode + "%");
		String projectCode = model.getProjectCode();
		if (projectCode != null)
			model.setProjectCode("%" + projectCode + "%");
		return dao.select(model);
	}
	/**
	 * 根据(逻辑)主键查询出唯一记录,主键不存在则返回null
	 * 
	 * @param model
	 * @return EmployeeModel
	 */
	public ScoreModel selectModel(ScoreModel model) {
		ScoreModel temp = new ScoreModel();
		temp.setEmployeeCode(model.getEmployeeCode());
		temp.setProjectCode(model.getProjectCode());
		List<ScoreModel> list = dao.select(temp);
		if(list == null || list.isEmpty()) 
			return null;
		return list.get(0);
	}
	/**
	 * 根据多条件的模糊查询出一些记录的条数count
	 * 
	 * @param model
	 * @return
	 */
	public int selectCount(ScoreModel model) {
		ScoreModel mdb = new ScoreModel();
		String employeeCode = model.getEmployeeCode();
		mdb.setEmployeeCode(employeeCode == null?"%%" : "%" + employeeCode + "%");
		String projectCode = model.getProjectCode();
		mdb.setProjectCode(projectCode == null?"%%" : "%" + projectCode + "%");
		return dao.selectCount(mdb);
	}
}
