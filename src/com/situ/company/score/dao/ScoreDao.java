package com.situ.company.score.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.situ.company.score.model.ScoreModel;
import com.situ.company.util.JDBCUtil;

public class ScoreDao {
	private static String table = "score";
	private static String cols = "employee_code,project_code,score";


	public Integer insert(ScoreModel model) {
		StringBuffer sql = new StringBuffer("insert into ");
		sql.append(table).append('(').append(cols).append(")values(?,?,?)");
		
		return JDBCUtil.update(sql.toString(), model.getEmployeeCode(),model.getProjectCode(),model.getScore());
	}
	
	public List<ScoreModel> select(ScoreModel model) {
		
		StringBuffer sql = new StringBuffer("select ");
		sql.append(cols).append(" from ").append(table);
		List<Object> vals = appendWhere(sql,model);
		Map<String,String> map = new HashMap<>();
		map.put("employeeCode", "employee_code");
		map.put("projectCode", "project_code");
		map.put("score", "score");
		return JDBCUtil.query(sql.toString(), vals, ScoreModel.class, map);
	}
	
	public Integer delete(ScoreModel model) {
		StringBuffer sql = new StringBuffer("delete from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		
		return JDBCUtil.update(sql.toString(), vals);
	}
	
	public Integer update(ScoreModel model) {
		StringBuffer sql = new StringBuffer("update ");
		sql.append(table);
		List<Object> vals = appendSet(sql,model);
		
		return JDBCUtil.update(sql.toString(), vals);
	}
	private List<Object> appendSet(StringBuffer sql,ScoreModel model){
		sql.append(" set id=id");
		List<Object> objs = new ArrayList<>();
		String employeeCode = model.getEmployeeCode();
		if(employeeCode != null ) {
			sql.append(" ,employee_code = ? ");
			objs.add(employeeCode);
		}
		String projectCode = model.getProjectCode();
		if(projectCode != null ) {
			sql.append(" ,project_code = ? ");
			objs.add(projectCode);
		}
		String score = model.getScore();
		if(score != null ) {
			sql.append(" ,score = ? ");
			objs.add(score);
		}
		
		sql.append(" where employee_code = ? and project_code = ? ");
		objs.add(employeeCode);
		objs.add(projectCode);
		return objs;
	}
	private List<Object> appendWhere(StringBuffer sql,ScoreModel model) {
		sql.append(" where 1=1 ");
		List<Object> objs = new ArrayList<>();
		String employeeCode = model.getEmployeeCode();
		if(employeeCode != null && !"".equals(employeeCode.trim())) {
			sql.append(" and employee_code like ? ");
			objs.add(employeeCode);
		}
		String projectCode = model.getProjectCode();
		if(projectCode != null && !"".equals(projectCode.trim())) {
			sql.append(" and project_code like ? ");
			objs.add(projectCode);
		}
		String score = model.getScore();
		if(score != null && !"".equals(score.trim())) {
			sql.append(" and score like ? ");
			objs.add(score);
		}
		String order = model.getOrderby();
		if(order != null && !"".equals(order.trim())) {
			sql.append(" order by "+order);
		}
		if(model.isPageOn()) {
			sql.append(" limit "+model.getRowStart()+","+model.getPageLimit());
		}
		
		
		return objs;
		
	}
	/**
	 * 分页查总数
	 * 
	 * @param mdb
	 * @return
	 */
	public int selectCount(ScoreModel mdb) {
		StringBuffer sql = new StringBuffer("select count(id) from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,mdb);
		
		return JDBCUtil.queryInt(sql.toString(), vals);
	}

}
