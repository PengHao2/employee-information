package com.situ.company.department.model;

import com.situ.company.util.PagerModel;

public class DepartmentModel extends PagerModel{

	private String code;
	private String name;
	private String tel;
	
	private int count;//部门人数
	
	public DepartmentModel() {
		super();
	}
	public DepartmentModel(String code, String name, String tel) {
		super();
		this.code = code;
		this.name = name;
		this.tel = tel;
	}
	
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}
