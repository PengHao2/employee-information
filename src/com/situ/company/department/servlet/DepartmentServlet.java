package com.situ.company.department.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.department.service.DepartmentService;
import com.situ.company.util.FmtRequest;

@WebServlet("/departmentServlet")
public class DepartmentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String result = "";
		String action = req.getParameter("action");
		switch (action) {
		case "add":
			result = add(req);
			break;
		case "sel":
			result = sel(req);
			break;
		case "del":
			result = del(req);
			break;
		case "get"://回显
			result = get(req);
			break;
		case "upd":
			result = upd(req);
			break;
		case "list":
			result = list(req);
			break;
		default:
			break;

		}

		FmtRequest.out(resp, result);

	}

	/**
	 * 分页
	 * 
	 * @param req
	 * @return
	 */
	private String list(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String pageIndex = req.getParameter("pageIndex");
		String pageLimit = req.getParameter("pageLimit");
		DepartmentModel model = new DepartmentModel(code, name, null);
		model.setPageIndex(Integer.parseInt(pageIndex));
		model.setPageLimit(Integer.parseInt(pageLimit));
		model.setPageOn(true);
		List<DepartmentModel> list = service.selectList(model);// 1,10
		// 注意这里list的size是分页后的条数,而要传回去的count是查出来的数据总条数
		int count = service.selectCount(model);// 这里我想直接给model设置不分页再selectList一次,然后取list.size(),是会因为传回来数据大影响效率
		
		Map<String,Object> map = new HashMap<>();
		  
		map.put("data", list); 
		map.put("count",count); 
		return new JSONObject(map).toString();
		 
		/*
		 * JSONObject obj = new JSONObject();//我有自己的想法(-.-) obj.put("data", list);
		 * obj.put("count", count); return obj.toString();
		 */
	}

	private String upd(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String tel = req.getParameter("tel");
		DepartmentModel model = new DepartmentModel(code, name, tel);
		return service.update(model);
	}

	private String get(HttpServletRequest req) {
		String code = req.getParameter("code");
		DepartmentModel model = new DepartmentModel(code, null, null);
		DepartmentModel result = service.selectModel(model);
		return new JSONObject(result).toString();
	}

	private DepartmentService service = new DepartmentService();

	private String del(HttpServletRequest req) {
		String code = req.getParameter("code");
		DepartmentModel model = new DepartmentModel(code, null, null);
		return service.delete(model);

	}

	private String sel(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		DepartmentModel model = new DepartmentModel(code, name, null);

		List<DepartmentModel> list = service.selectList(model);

		return new JSONArray(list).toString();

	}

	/**
	 * 返回E编号重复,返回1添加成功
	 * 
	 * @param req
	 * @return
	 */
	private String add(HttpServletRequest req) {
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		String tel = req.getParameter("tel");
		DepartmentModel model = new DepartmentModel(code, name, tel);
		return service.insert(model);

	}
}
