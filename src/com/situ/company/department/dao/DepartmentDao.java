package com.situ.company.department.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.situ.company.department.model.DepartmentModel;
import com.situ.company.util.JDBCUtil;



public class DepartmentDao {

	private static String table = "department";
	private static String cols = "code,name,tel";
	private Connection conn;
	private PreparedStatement ps;

	public Integer insert(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("insert into ");
		sql.append(table).append('(').append(cols).append(")values(?,?,?)");
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			ps.setString(1, model.getCode());
			ps.setString(2, model.getName());
			ps.setString(3, model.getTel());
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	
	public List<DepartmentModel> select(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("select ");
		sql.append(cols).append(",(select count(id) from employee where department_code=department.code) count").append(" from ").append(table);
		List<Object> vals = appendWhere(sql,model);
		ResultSet rs =null;
		List<DepartmentModel> list = new ArrayList<>();
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				DepartmentModel mdb = new DepartmentModel();
				mdb.setCode(rs.getString("code"));
				mdb.setName(rs.getString("name"));
				mdb.setTel(rs.getString("tel"));
				mdb.setCount(rs.getInt("count"));
				list.add(mdb);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps, rs);
		}
		return list;
	}
	/**
	 * 分页查询分页控件的count参数
	 * 
	 * @param model
	 * @return
	 */
	public int selectCount(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("select count(id) from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		ResultSet rs =null;
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			rs = ps.executeQuery();
			if(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps, rs);
		}
		return 0;
	}
	
	public Integer delete(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("delete from ");
		sql.append(table);
		List<Object> vals = appendWhere(sql,model);
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	
	public Integer update(DepartmentModel model) {
		StringBuffer sql = new StringBuffer("update ");
		sql.append(table);
		List<Object> vals = appendSet(sql,model);
		try {
			conn = JDBCUtil.getConnection();
			ps = conn.prepareStatement(sql.toString());
			for(int i = 0;i<vals.size();i++) {
				ps.setObject(i+1, vals.get(i));
			}
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(conn, ps);
		}
		return null;
	}
	private List<Object> appendSet(StringBuffer sql,DepartmentModel model){
		sql.append(" set id=id");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null ) {
			sql.append(" ,code = ? ");
			objs.add(code);
		}
		String name = model.getName();
		if(name != null ) {
			sql.append(", name = ? ");
			objs.add(name);
		}
		
		String tel = model.getTel();
		if(tel != null ) {
			sql.append(" ,tel = ? ");
			objs.add(tel);
		}
		sql.append(" where code = ?");
		objs.add(code);
		return objs;
	}
	private List<Object> appendWhere(StringBuffer sql,DepartmentModel model) {
		sql.append(" where 1=1 ");
		List<Object> objs = new ArrayList<>();
		String code = model.getCode();
		if(code != null && !"".equals(code.trim())) {
			sql.append(" and code like ? ");
			objs.add(code);
		}
		String name = model.getName();
		if(name != null && !"".equals(name.trim())) {
			sql.append(" and name like ? ");
			objs.add(name);
		}
		
		String tel = model.getTel();
		if(tel != null && !"".equals(tel.trim())) {
			sql.append(" and tel like ? ");
			objs.add(tel);
		}
		String order = model.getOrderby();
		if(order != null && !"".equals(order.trim())) {
			sql.append(" order by "+order);//这里不能使用?再填值的方法,因为预处理为了防止sql注入会把?用引号引起来,这样sql语句就错了
		}
		if(model.isPageOn()) {//这里直接把limit拼完,不用setObject
			sql.append(" limit "+model.getRowStart()+","+model.getPageLimit());
		}
		return objs;
		
	}

}
