package com.situ.company.department.service;

import java.util.List;

import com.situ.company.department.dao.DepartmentDao;
import com.situ.company.department.model.DepartmentModel;
import com.situ.company.employee.dao.EmployeeDao;
import com.situ.company.employee.model.EmployeeModel;

public class DepartmentService {
	private DepartmentDao dao = new DepartmentDao();
	private EmployeeDao employeeDao = new EmployeeDao();
	public String insert(DepartmentModel model) {
		DepartmentModel mdb = selectModel(model);
		if(mdb != null) {
			return "E";
		}
		Integer result = dao.insert(model);
		return String.valueOf(result);
		
	}
	/**
	 * 删除部门,有员工返回E
	 * @param model
	 * @return
	 */
	public String delete(DepartmentModel model) {
		EmployeeModel m0 = new EmployeeModel();
		m0.setDepartmentCode(model.getCode());
		int count = employeeDao.selectCount(m0);
		if(count > 0) {
			return "E"; 
		}
		Integer result = dao.delete(model);
		return String.valueOf(result);
		
	}
	public String update(DepartmentModel model) {
		Integer result = dao.update(model);
		return String.valueOf(result);
		
	}
	/**
	 * 根据多条件的模糊查询出一些记录
	 * 
	 * @param model
	 * @return List<EmployeeModel>
	 */
	public List<DepartmentModel> selectList(DepartmentModel model) {
		String code = model.getCode();
		if (code != null)
			model.setCode("%" + code + "%");
		String name = model.getName();
		if (name != null)
			model.setName("%" + name + "%");
		return dao.select(model);
	}
	/**
	 * 根据(逻辑)主键查询出唯一记录,主键不存在则返回null
	 * 
	 * @param model
	 * @return EmployeeModel
	 */
	public DepartmentModel selectModel(DepartmentModel model) {
		DepartmentModel temp = new DepartmentModel();
		temp.setCode(model.getCode());
		List<DepartmentModel> list = dao.select(temp);
		if(list == null || list.isEmpty()) 
			return null;
		return list.get(0);
	}
	/**
	 * 根据多条件的模糊查询出一些记录的条数count
	 * 
	 * @param model
	 * @return
	 */
	public int selectCount(DepartmentModel model) {
		DepartmentModel mdb = new DepartmentModel();
		String code = model.getCode();
//		if (code != null)
//			model.setCode("%" + code + "%");
		mdb.setCode(code == null?"%%" : "%" + code + "%");
		String name = model.getName();
//		if (name != null)
//			model.setName("%" + name + "%");
		mdb.setName(name == null?"%%" : "%" + name + "%");
		return dao.selectCount(mdb);
	}
}
