package com.situ.company.init;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
@WebFilter("/web/page/*")
public class UserFilter implements Filter {
	// 防盗链
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpSession session = request.getSession();
		Object user = session.getAttribute("user");
		if (user == null) {
			 request.getRequestDispatcher("/web/login.jsp").forward(req, resp);
			// 跳转页面
//			 request.getRequestDispatcher("").forward(req, resp);
//			 ((HttpServletResponse)resp).sendRedirect("");
		} else {
			chain.doFilter(req, resp);
		}

	}

}
