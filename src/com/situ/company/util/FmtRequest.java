package com.situ.company.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FmtRequest {
	/**
	 * 
	 * @param <T>
	 * @param req
	 * @param clazz
	 * @param fields
	 * 			 key=属性名 value=参数名
	 * @return
	 */
	public static <T> T parseModel(HttpServletRequest req,Class<T> clazz,Map<String,String> fields) {
		T obj = null;
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
		for (Entry<String,String> entry:fields.entrySet()) {
			try {
				Field field = clazz.getDeclaredField(entry.getKey());
				field.setAccessible(true);
				field.set(obj,req.getParameter(entry.getValue()));
				
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
				return null;
			}
			
		}
		return obj;
	}
	/**
	 * 属性名和参数名一致时不需要传映射的封装对象方法
	 * 
	 * @param <T>
	 * @param req
	 * @param clazz
	 * @param fields
	 * 			 key=属性名 value=参数名
	 * @return
	 */
	public static <T> T parseModel(HttpServletRequest req,Class<T> clazz) {
		T obj = null;
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
		Map<String, String[]> map = req.getParameterMap();
//		for (Entry<String, String[]> entry:map.entrySet()) {
//			System.out.println(entry.getKey()+"="+entry.getValue()[0]);
//			
//		}
		for (Entry<String, String[]> entry:map.entrySet()) {
			if("action".equals(entry.getKey())||"pageIndex".equals(entry.getKey())||"pageLimit".equals(entry.getKey()))
				continue;
			
			try {
				Field field = clazz.getDeclaredField(entry.getKey());
				field.setAccessible(true);
				field.set(obj,entry.getValue()[0]);
				
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
				return null;
			}
		}
		return obj;
	}

	public static void out(HttpServletResponse resp, String result) throws IOException {
		PrintWriter out = resp.getWriter();
		out.append(result);
		out.flush();
		out.close();
		out = null;
		
	}
	
}
