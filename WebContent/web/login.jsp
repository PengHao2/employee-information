<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>系统登录</title>
</head>
<body>
<!-- http://127.0.0.1:8080/EmployeeInformation/web/login.jsp -->
<fieldset class="layui-elem-field">
	<legend>系统登录</legend>
	<div class="layui-field-box">
		<form class="layui-form layui-form-pane" >
			<div class="layui-form-item">
				<label class="layui-form-label">账户</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" 
      				placeholder="请输入账户" autocomplete="off" class="layui-input">
    			</div>
			</div>
			<div class="layui-form-item">
    			<label class="layui-form-label">密码</label>
    			<div class="layui-input-inline">
     				<input type="password" name="pass" required lay-verify="required" 
     				placeholder="请输入密码" autocomplete="off" class="layui-input">
   	 			</div>
   	 		</div>
   	 		
			
      				
			
			
			<div class="layui-form-item">
				<label class="layui-form-label">
					<img src='/EmployeeInformation/authCodeServlet' 
					onclick="this.src='/EmployeeInformation/authCodeServlet?'+Math.random();" />
				</label>
    			<div class="layui-input-inline">
      				<input type="text" name="authCode" required  lay-verify="required" 
      				placeholder="请输入验证码" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="登录" class="layui-btn" lay-submit lay-filter="login" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="注册" class="layui-btn" onclick="toJspReg()"/>
  			</div>
  			
  			<input type="hidden" name="action" value="login" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
/* var form = layui.form;
var $ = layui.jquery;
var layer = layui.layer; */
/* form.on('submit(login)',function(data){
	//console.log(data);
	
 	$.ajax({
		type:"post",
		data:data.field,
		url:base.app+"/employeeServlet",
		dataType:"text",
		success:function(data){
			if("A" == data){
				layer.msg("验证码错误");
			}else if("0"== data){
				layer.msg("账号不存在");
			}else if("2" == data){
				layer.msg("密码错误");
			}else if("1" == data){
				layer.msg("登录成功");
			}
			
		}
	});  
}) */
formOn('submit(login)','/employeeServlet','text',function(data){
	if("A" == data){
		layer.msg("验证码错误");
	}else if("0"== data){
		layer.msg("账号不存在");
	}else if("2" == data){
		layer.msg("密码错误");
	}else if("1" == data){
		layer.msg("登录成功",{time:1000},function(){
			
			toJsp('/web/page/main.jsp');
		});
		
	}
})
</script>









</body>
</html>