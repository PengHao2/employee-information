<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>系统注册</title>
<!-- http://127.0.0.1:8080/EmployeeInformation/web/reg.jsp -->
</head>
<body>
<fieldset class="layui-elem-field">
	<legend>系统注册</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" >
			<div class="layui-form-item">
				<label class="layui-form-label">账户</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" 
      				placeholder="请输入账户" autocomplete="off" class="layui-input">
    			</div>
			</div>
			<div class="layui-form-item">
    			<label class="layui-form-label">密码</label>
    			<div class="layui-input-inline">
     				<input type="password" name="pass" 
     				placeholder="请输入密码" autocomplete="off" class="layui-input">
   	 			</div>
   	 		</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">姓名</label>
    			<div class="layui-input-inline">
      				<input type="text" name="name" required  lay-verify="required" 
      				placeholder="请输入姓名" autocomplete="off" class="layui-input">
    			</div>
			</div>
			
			
      				
			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
    			<div class="layui-input-block">
      				<input type="radio" name="gender" value="男" title="男">
					<input type="radio" name="gender" value="女" title="女" checked>
    			</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">入职时间</label>
    			<div class="layui-input-inline">
      				<input type="text" class="layui-input" id="test1" name="entryTime">
    			</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">
					<img src='/EmployeeInformation/authCodeServlet' 
					onclick="this.src='/EmployeeInformation/authCodeServlet?'+Math.random();" />
				</label>
    			<div class="layui-input-inline">
      				<input type="text" name="authCode" required  lay-verify="required" 
      				placeholder="请输入验证码" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="注册" class="layui-btn" lay-submit lay-filter="reg" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="返回登录" class="layui-btn" onclick="toJspLogin()"/>
  			</div>
  			<input type="hidden" name="action" value="reg" />
		</form>
	</div>
</fieldset>
<script>
layui.use('laydate', function(){
  var laydate = layui.laydate;
  
  //执行一个laydate实例
  laydate.render({
    elem: '#test1' //指定元素
    ,trigger: 'click'
  });
});
</script>
<script type="text/javascript">
/* var form = layui.form;

var $ = layui.jquery;
var layer = layui.layer;
form.on('submit(reg)',function(data){
	//console.log(data);
	
 	$.ajax({
		type:"post",
		data:data.field,
		url:"/EmployeeInformation/employeeServlet",
		dataType:"text",
		success:function(data){
			if('A'==data){
				layer.msg("验证码错误!");
			}else if(1==data){
				layer.msg("注册成功");
			}else if('E' == data){
				layer.msg("账户重复")			
			}else{
				layer.msg("注册失败");
			}
			
		}
	}); 
}) */
form.render();
formOn('submit(reg)','/employeeServlet','text',function(data){
	if('A'==data){
		layer.msg("验证码错误!");
	}else if(1==data){
		layer.msg("注册成功");
	}else if('E' == data){
		layer.msg("账户重复")			
	}else{
		layer.msg("注册失败");
	}
})
</script>















</body>
</html>