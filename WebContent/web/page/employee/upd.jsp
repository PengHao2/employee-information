<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>信息维护</title>
</head>
<body>

<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" lay-filter="formA">
			<div class="layui-form-item">
				<label class="layui-form-label">账号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" readonly
      				placeholder="请输入账户" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">姓名</label>
    			<div class="layui-input-inline">
      				<input type="text" name="name" required  lay-verify="required" 
      				placeholder="请输入姓名" autocomplete="off" class="layui-input">
    			</div>
			</div>
			
			
      				
			<div class="layui-form-item">
				<label class="layui-form-label">性别</label>
    			<div class="layui-input-block">
      				<input type="radio" name="gender" value="男" title="男">
					<input type="radio" name="gender" value="女" title="女" checked>
    			</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">入职时间</label>
    			<div class="layui-input-inline">
      				<input type="text" class="layui-input" id="test1" name="entryTime">
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="修改" class="layui-btn" lay-submit lay-filter="upd" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="upd" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
element.render();
form.render();
layui.use('laydate', function(){
	  var laydate = layui.laydate;
	  
	  //执行一个laydate实例
	  laydate.render({
	    elem: '#test1' //指定元素
	    ,trigger: 'click'
	  });
	});
function init(){//回显
	var code = "<%=request.getParameter("code") %>";
	ajax("/employeeServlet",{code:code,action:"get"},"json",function(data){
		form.val("formA",data);//回显赋值
	})
}
init();
formOn('submit(upd)','/employeeServlet','text',function(data){
	console.log(data);
	if(1==data){
		layer.msg("修改成功",{time:1000},toClose);		
	}else{
		layer.msg("修改失败");
	}
})

</script>
</body>
</html>