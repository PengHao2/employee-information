 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>修改密码</title>
</head>
<body>
<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" lay-filter="formA">
			<div class="layui-form-item">
				<label class="layui-form-label">编号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" readonly
      				placeholder="请输入账户" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">新密码</label>
    			<div class="layui-input-inline">
      				<input type="password" name="pass" 
      				placeholder="请输入新密码" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="修改" class="layui-btn" lay-submit lay-filter="upd" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="updPass" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
element.render();
form.render();
function init(){
	var code = <%= request.getParameter("code")%>;
	$("input[name='code']").val(code)
}
init();
formOn('submit(upd)','/employeeServlet','text',function(data){
	console.log(data);
	if(1==data){
		layer.msg("密码修改成功",{time:1000},toClose);		
	}else{
		layer.msg("密码修改失败");
	}
})

</script>
</body>
</html>