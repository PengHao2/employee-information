<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>修改部门</title>
</head>
<body>
<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" lay-filter="formA">
			<div class="layui-form-item">
				<label class="layui-form-label">编号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" readonly
      				placeholder="请输入账户" autocomplete="off" class="layui-input">
    			</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">姓名</label>
    			<div class="layui-input-inline">
      				<input type="text" name="name" required  lay-verify="required" readonly
      				placeholder="请输入姓名" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">部门</label>
    			<div class="layui-input-inline">
      				<select name="departmentCode"></select>
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="修改" class="layui-btn" lay-submit lay-filter="updDepartment" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="updDepartment" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
initSelect();
function initSelect(){
	ajax('/departmentServlet',{action:"sel"},'json',function(data){
		var html = '<option value=""></option>';
		$.each(data,function(index,element){
			html += '<option value="'+this.code+'">'+this.name+'</option>';
		})
		$("select[name='departmentCode']").html(html);
		form.render();
		init();
	});
}
function init(){
	var code = '<%= request.getParameter("code")%>';
	ajax('/employeeServlet',{code:code,action:'get'},'json',function(data){
		form.val("formA",data);
	});
}
formOn('submit(updDepartment)','/employeeServlet','text',function(data){
	console.log(data);
	if(1==data){
		layer.msg("部门修改成功",{time:1000},toClose);		
	}else{
		layer.msg("部门修改失败");
	}
})

</script>
</body>
</html>