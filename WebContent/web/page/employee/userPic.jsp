<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>图片维护</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin:10px;padding:15px">
  	<legend>图片维护</legend>
  	<div class="layui-field-box">
		<button type="button" class="layui-btn" id="test1">
 		 <i class="layui-icon layui-icon-upload"></i>上传图片
		</button>
		<div class="layui-upload-list">
    		<table class="layui-table">
    	  		<thead>
      	  			<tr>
       		 			<th>文件名</th>
        				<th>图片</th>
        				<th>操作</th>
     	 			</tr>
      	  		</thead>
      	  		<tbody id="demoList"></tbody>
    		</table>
 		</div>
  	</div>
</fieldset>
<script type="text/javascript">
var code = '<%=request.getParameter("code")%>';
renderUpload("test1","/employeeUploadServlet",{code:code},function(res,index,upload){
	if(res.code == 1){
		layer.msg("上传成功",{time:1000})//do something （比如将res返回的图片链接保存到表单的隐藏域）
		showPic(res.image);
	}
})
function showPic(image){
	var html = '<tr>'
	+"<td>"+image+"</td>"
	+"<td><img src='/image/"+image+"' class='layui-upload-img'></td>"
	+"<td><input type='button' value='删除' class='layui-btn ' onclick='delPic()'></td>"
	+"</tr>";
	$("#demoList").html(html);
}
function delPic(){
	openConfirm(function(){
		ajax('/employeeServlet',{code:code,action:'delPic'},'text',function(data){
			if(data==1){
				layer.msg("删除成功",{time:1000});
				//init();
				$("#demoList").html("");
			}else{
				layer.msg("删除失败",{time:1000});
			}
		})
	},"是否删除该图片?")
}
init();
function init(){
	ajax('/employeeServlet',{code:code,action:'get'},'json',function(data){
		var image = data.image;
		if(image)
			showPic(image);
	})
}
</script>
</body>
</html>