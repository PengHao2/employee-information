<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>员工列表</title>
</head>
<body>
<div class="layui-collapse">
  <div class="layui-colla-item">
    <h2 class="layui-colla-title">员工信息</h2>
    <div class="layui-colla-content layui-show">
    	<fieldset class="layui-elem-field layui-field-title">
  			<legend>查询条件</legend>
  			<div class="layui-field-box">
				<form class="layui-form layui-form-pane" >
					<div class="layui-form-item">
						<label class="layui-form-label">员工编号</label>
    					<div class="layui-input-inline">
      						<input type="text" name="code" placeholder="请输入员工编号" autocomplete="off" class="layui-input">
    					</div>
						<label class="layui-form-label">员工姓名</label>
    					<div class="layui-input-inline">
      						<input type="text" name="name" placeholder="请输入员工姓名" autocomplete="off" class="layui-input">
    					</div>
    					<div class="layui-input-inline">
      						<input type="button" value='查询' class="layui-btn"  lay-submit lay-filter="sel"/>
      						<input type="reset" value="重置" class="layui-btn layui-btn-primary"  />
      						
    					</div>
    					<input type="button" value="添加" class="layui-btn " onclick="openAdd()" />
  					</div>
  					<input type="hidden" name="action" value="list" />
  					<input type="hidden" name="pageIndex" value='1' />
  					<input type="hidden" name="pageLimit" value='10' />
				</form>
  			</div>
		</fieldset>
	</div>
  </div>
</div>

<table class="layui-table">
  <colgroup>
    <col width="5%">
    <col width="10%"><col width="10%"><col width="5%"><col width="10%"><col width="10 %"><col width="15%">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>序号</th>
      <th>编号</th>
      <th>姓名</th>
      <th>性别</th>
      <th>入职日期</th>
      <th>部门编号</th>
      <th>部门名称</th>
      <th>操作</th>
    </tr> 
  </thead>
  <tbody id="user_tbody"></tbody>
</table>
<div id="pageInfo" style="text-align:right;padding-right: 30px"></div>
<script>
element.render();




formOn('submit(sel)','/employeeServlet','json',function(data){
	
	var pageIndex = $("input[name='pageIndex']").val();
	var pageLimit = $("input[name='pageLimit']").val();
	setPageInfo("pageInfo",data.count,pageIndex,pageLimit,function(obj,first){
		$("input[name='pageIndex']").val(obj.curr);
		$("input[name='pageLimit']").val(obj.limit);
		if(! first){refresh()}
	})
	var html = "";
	 $.each(data.data,function(index,element){
		element.id = (index+1)+(pageIndex-1)*pageLimit;
		html+=getLaytpl("#tradd",element);
		//html+=laytpl($("#tradd").html()).render(element);
	}) 
	$("#user_tbody").html(html);
})
refresh();
function refresh(){
	$("input[value='查询']").click();
}
function openAdd(){
	openLayer("/web/page/employee/add.jsp",refresh);
}
function del(code){
	openConfirm(function(index){
		ajax("/employeeServlet",{code:code,action:"del"},"text",function(data){
			if(1==data){
				layer.msg("删除成功",{time:1000},refresh);
				
			}else{
				layer.msg("删除失败");
			}
		});
	},"确定是否删除该员工?")
	/* layer.confirm("确定进行该操作?",{icon:3,title:"提示"},function(index){
		ajax("/employeeServlet",{code:code,action:"del"},"text",function(data){
			if(1==data){
				layer.msg("删除成功",{time:1000},refresh);
				
			}else{
				layer.msg("删除失败");
			}
		});
	}); */
}
function openUpd(code){
	openLayer("/web/page/employee/upd.jsp?code="+code,refresh);
}
function resetPass(code){
	openConfirm(function(index){
		ajax("/employeeServlet",{code:code,action:"resetPass"},"text",function(data){
			console.log(data);
			if(1==data){
				layer.msg("密码重置成功",{time:1000},refresh);
				
			}else{
				layer.msg("密码重置失败");
			}
		});
	},"确定重置密码?")
	/* layer.confirm("确定重置密码?",{icon:3,title:"提示"},function(index){
		ajax("/employeeServlet",{code:code,action:"resetPass"},"text",function(data){
			console.log(data);
			if(1==data){
				layer.msg("密码重置成功",{time:1000},refresh);
				
			}else{
				layer.msg("密码重置失败");
			}
		});
	}); */
}
function updPass(code){
	openLayer("/web/page/employee/updPass.jsp?code="+code,refresh);
}
function updDepartment(code){
	openLayer("/web/page/employee/updDepartment.jsp?code="+code,refresh);
}
function setPic(code){
	openLayer("/web/page/employee/userPic.jsp?code="+code,refresh);
}
</script>
<script type="text/html" id="tradd">
<tr>
	<td>{{d.id}}</td>
	<td>{{d.code}}</td>
	<td>{{d.name}}</td>
	<td>{{d.gender}}</td>
	<td>{{d.entryTime}}</td>
	<td>{{d.departmentCode==null?"":d.departmentCode}}</td>
	<td>{{d.departmentName==null?"":d.departmentName}}</td>
	<td>
		<a href="javascript:del('{{d.code}}');" class="layui-btn layui-btn-danger layui-btn-sm" >
			<i class="layui-icon layui-icon-delete"></i>
		</a>
		<input type="button" value="修改" class="layui-btn layui-btn-sm" onclick="openUpd('{{d.code}}')" />
		<input type="button" value="密码重置" class="layui-btn layui-btn-sm" onclick="resetPass('{{d.code}}')" />
		<input type="button" value="密码修改" class="layui-btn layui-btn-sm" onclick="updPass('{{d.code}}')" />
		<input type="button" value="部门修改" class="layui-btn layui-btn-sm" onclick="updDepartment('{{d.code}}')" />
		<input type="button" value="上传头像" class="layui-btn layui-btn-sm" onclick="setPic('{{d.code}}')" />
	</td>
</tr>
</script>
</body>
</html>