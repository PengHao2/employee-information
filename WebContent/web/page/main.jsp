<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<!-- http://127.0.0.1:8080/EmployeeInformation/web/page/main.jsp -->
<%@ include file="/web/header.jsp" %>
<title>主界面</title>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo">大风厂</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left"></ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item">
        <a href="javascript:;">
          <img src="<%=app %>/web/base/img/Ace.jpg" class="layui-nav-img">
          <img src="/image/${user.image}" class="layui-nav-img">
          	${ user.name }
        </a>
        <dl class="layui-nav-child">
          <dd><a href="">修改资料</a></dd>
          <dd><a href="">修改密码</a></dd>
          <dd><a href="">修改头像</a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="javascript:toLogout();">注销</a></li>
    </ul>
  </div>
  
  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
          <a class="" href="javascript:;">信息维护</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/page/department/list.jsp');">部门信息维护</a></dd>
            <dd><a href="javascript:openUrl('/web/page/employee/list.jsp');">员工信息维护</a></dd>
            <dd><a href="javascript:openUrl('/web/page/project/list.jsp');">项目信息维护</a></dd>
            <dd><a href="javascript:openUrl('/web/page/score/list.jsp');">绩效信息维护</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">部门信息</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/page/department/list.jsp');">部门信息维护 </a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">员工信息</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/page/employee/list.jsp');">员工信息维护 </a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">项目信息</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/page/project/list.jsp');">项目信息维护 </a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">绩效信息</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/page/score/list.jsp');">绩效信息维护 </a></dd>
          </dl>
        </li>
         <li class="layui-nav-item">
          <a href="javascript:;">其他</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:openUrl('/web/404.jsp');">测试1 </a></dd>
          </dl>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;" class="demoA" data-url="/web/404.jsp">测试 2</a></dd>
          </dl>
        </li>
      </ul>
    </div>
  </div>
  
  <div class="layui-body"><!-- 内容主体区域 -->
    <iframe name="frameA" width="100%" height="98%"></iframe>
  </div>
  
  <div class="layui-footer"><!-- 底部固定区域 -->© layui.com - 底部固定区域</div>
</div>

<script>
element.render();
$('.demoA').click(function(){
	window.open(base.app+$(this).data('url'),"frameA");
})
function openUrl(url){
	window.open(base.app+url,"frameA");
}
function toLogout(){
	openConfirm(function(index){
		//location.href = base.app+'/employeeServlet?action=logout'
		toJsp('/employeeServlet?action=logout');
	},"是否进行注销?");
}
 

</script>
</body>
</html>