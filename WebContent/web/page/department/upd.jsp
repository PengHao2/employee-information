<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>信息维护</title>
</head>
<body>

<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" lay-filter="formA">
			<div class="layui-form-item">
				<label class="layui-form-label">编号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="code" required  lay-verify="required" readonly
      				placeholder="请输入编号" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">名称</label>
    			<div class="layui-input-inline">
      				<input type="text" name="name" required  lay-verify="required" 
      				placeholder="请输入名称" autocomplete="off" class="layui-input">
    			</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">电话</label>
    			<div class="layui-input-inline">
      				<input type="text" name="tel" 
      				placeholder="请输入电话" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="修改" class="layui-btn" lay-submit lay-filter="upd" />
      				<input type="button" value="重置" class="layui-btn layui-btn-primary" onclick="init()"/>
      				
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="upd" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
element.render();
function init(){//回显
	var code = "<%=request.getParameter("code") %>";
	ajax("/departmentServlet",{code:code,action:"get"},"json",function(data){
		form.val("formA",data);
		/* $("input[name='code']").val(data.code);
		$("input[name='name']").val(data.name);
		$("input[name='tel']").val(data.tel); */
	})
}
init();
formOn('submit(upd)','/departmentServlet','text',function(data){
	//console.log(data);
	if(1==data){
		layer.msg("修改成功",{time:2000},toClose);		
	}else{
		layer.msg("修改失败");
	}
})

</script>
</body>
</html>