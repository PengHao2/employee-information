<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>Insert title here</title>
</head>
<body>
<div class="layui-collapse">
  <div class="layui-colla-item">
    <h2 class="layui-colla-title">部门信息</h2>
    <div class="layui-colla-content layui-show">
    	<fieldset class="layui-elem-field layui-field-title">
  			<legend>查询条件</legend>
  			<div class="layui-field-box">
				<form class="layui-form layui-form-pane" >
					<div class="layui-form-item">
						<label class="layui-form-label">部门编号</label>
    					<div class="layui-input-inline">
      						<input type="text" name="code" placeholder="请输入部门编号" autocomplete="off" class="layui-input">
    					</div>
						<label class="layui-form-label">部门名称</label>
    					<div class="layui-input-inline">
      						<input type="text" name="name" placeholder="请输入部门名称" autocomplete="off" class="layui-input">
    					</div>
    					<div class="layui-input-inline">
      						<input type="button" value='查询' class="layui-btn"  lay-submit lay-filter="sel"/>
      						<input type="reset" value="重置" class="layui-btn layui-btn-primary"  />
      						
    					</div>
    					<input type="button" value="添加" class="layui-btn " onclick="openAdd()" />
  					</div>
  					<input type="hidden" name="action" value="list" />
  					<input type="hidden" name="pageIndex" value='1' />
  					<input type="hidden" name="pageLimit" value='10' />
				</form>
  			</div>
		</fieldset>
	</div>
  </div>
</div>

<table class="layui-table">
  <colgroup>
    <col width="10%">
    <col width="10%"><col width="20%"><col width="20%"><col width="10%">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>序号</th>
      <th>编号</th>
      <th>名称</th>
      <th>电话</th>
      <th>人数</th>
      <th>操作</th>
    </tr> 
  </thead>
  <tbody id="department_tbody"></tbody>
</table>
<div id="pageInfo" style="text-align:right;padding-right: 30px"></div>
<script>
element.render();




formOn('submit(sel)','/departmentServlet','json',function(data){
	console.log(data);
	//console.log(data.count);
	var pageIndex = $("input[name='pageIndex']").val();
	var pageLimit = $("input[name='pageLimit']").val();
	setPageInfo("pageInfo",data.count,pageIndex,pageLimit,function(obj,first){
		$("input[name='pageIndex']").val(obj.curr);
		$("input[name='pageLimit']").val(obj.limit);
		if(! first){refresh()}//首次不执行
	})
	/* laypage.render({
		elem:"pageInfo",
		count:data.count,
		limits:[10,20,30,40,50,60,70,],
		layout:['count','prev','page','next','limit','refresh','skip'],
		curr:pageIndex,
		limit:pageLimit,
		jump:function(obj,first){
			$("input[name='pageIndex']").val(obj.curr);
			$("input[name='pageLimit']").val(obj.limit);
			if(! first){refresh()}//首次不执行
		}
	}) */
	
	var html = "";
	 $.each(data.data,function(index,element){
		element.id = (index+1)+(pageIndex-1)*pageLimit;
		html += getLaytpl("#tradd",element);
	}) 
	$("#department_tbody").html(html);
})
/* formOn('submit(sel)','/departmentServlet','json',function(data){
	//console.log(data);
	var html = "";
	 $.each(data,function(index,element){
		element.id = index+1;
		html+=laytpl($("#tradd").html()).render(element);
	}) 
	$("#department_tbody").html(html);
}) */
refresh();
function refresh(){
	$("input[value='查询']").click();
}
function openAdd(){
/* 	layer.open({
		type:2,//iframe
		area:['750px','550px'],
		maxmin:true,
		content:base.app+"/web/page/department/add.jsp",
	}) */
	openLayer("/web/page/department/add.jsp",refresh);
}
function del(code){
	openConfirm(function(index){
		ajax("/departmentServlet",{code:code,action:"del"},"text",function(data){
			if(1==data){
				layer.msg("删除成功",{time:2000},refresh);
				
			}else if("E" == data){
				layer.msg("要删除的部门有员工,删除失败");
			}else{
				layer.msg("删除失败");
			}
		});
	},"确定删除该部门?")
	/* layer.confirm("确定进行该操作?",{icon:3,title:"提示"},function(index){
		ajax("/departmentServlet",{code:code,action:"del"},"text",function(data){
			if(1==data){
				layer.msg("删除成功",{time:2000},refresh);
				
			}else if("E" == data){
				layer.msg("要删除的部门有员工,删除失败");
			}else{
				layer.msg("删除失败");
			}
		});
	}); */
}
function openUpd(code){
/* 	layer.open({
		type:2,//iframe
		area:['750px','550px'],
		maxmin:true,
		content:base.app+"/web/page/department/upd.jsp?code="+code,
	}) */
	openLayer("/web/page/department/upd.jsp?code="+code,refresh);
}
</script>
<script type="text/html" id="tradd">
<tr>
	<td>{{d.id}}</td>
	<td>{{d.code}}</td>
	<td>{{d.name}}</td>
	<td>{{d.tel}}</td>
	<td>{{d.count}}</td>
	<td>
		<a href="javascript:del('{{d.code}}');" class="layui-btn layui-btn-danger" >
			<i class="layui-icon layui-icon-delete"></i>
		</a>
		&nbsp;
		<input type="button" value="修改" class="layui-btn " onclick="openUpd('{{d.code}}')" />
	</td>
</tr>
</script>
</body>
</html>