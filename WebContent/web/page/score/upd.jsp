<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>信息维护</title>
</head>
<body>
<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" lay-filter="formA">
			<div class="layui-form-item">
				<label class="layui-form-label">员工编号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="employeeCode" required  lay-verify="required" readonly
      				placeholder="请输入员工编号" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">项目编号</label>
    			<div class="layui-input-inline">
      				<input type="text" name="projectCode" required  lay-verify="required" readonly
      				placeholder="请输入项目编号" autocomplete="off" class="layui-input">
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">绩效</label>
    			<div class="layui-input-inline">
      				<input type="text" name="score" required  lay-verify="required"
      				placeholder="请输入绩效" autocomplete="off" class="layui-input">
    			</div>
			</div>
			
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="修改" class="layui-btn" lay-submit lay-filter="upd" />
      				<input type="button" value="重置" class="layui-btn layui-btn-primary" onclick="init()"/>
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="upd" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
element.render();
form.render();
init();
function init(){
	var employeeCode = "<%=request.getParameter("employeeCode") %>";
	var projectCode = "<%=request.getParameter("projectCode") %>";
	ajax("/scoreServlet",{employeeCode:employeeCode,projectCode:projectCode,action:"get"},"json",function(data){
		form.val("formA",data);//回显赋值
	})
}
formOn('submit(upd)','/scoreServlet','text',function(data){
	//console.log(data)
	if(1==data){
		layer.msg("修改成功",{time:1000},toClose);			
	}else{
		layer.msg("修改失败");
	}
})
</script>
</body>
</html>