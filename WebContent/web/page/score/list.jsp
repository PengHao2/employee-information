<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>绩效信息</title>
</head>
<body>
<div class="layui-collapse">
  <div class="layui-colla-item">
    <h2 class="layui-colla-title">绩效信息</h2>
    <div class="layui-colla-content layui-show">
    	<fieldset class="layui-elem-field layui-field-title">
  			<legend>查询条件</legend>
  			<div class="layui-field-box">
				<form class="layui-form layui-form-pane" >
					<div class="layui-form-item">
						<label class="layui-form-label">员工编号</label>
    					<div class="layui-input-inline">
      						<input type="text" name="employeeCode" placeholder="请输入员工编号" autocomplete="off" class="layui-input">
    					</div>
						<label class="layui-form-label">项目编号</label>
    					<div class="layui-input-inline">
      						<input type="text" name="projectCode" placeholder="请输入项目编号" autocomplete="off" class="layui-input">
    					</div>
    					<div class="layui-input-inline">
      						<input type="button" value='查询' class="layui-btn"  lay-submit lay-filter="sel"/>
      						<input type="reset" value="重置" class="layui-btn layui-btn-primary"  />
      						
    					</div>
    					<input type="button" value="添加" class="layui-btn " onclick="openAdd()" />
  					</div>
  					<input type="hidden" name="action" value="list" />
  					<input type="hidden" name="pageIndex" value='1' />
  					<input type="hidden" name="pageLimit" value='10' />
				</form>
  			</div>
		</fieldset>
	</div>
  </div>
</div>

<table class="layui-table">
  <colgroup>
    <col width="5%">
    <col width="20%"><col width="20%"><col width="20%">
    <col>
  </colgroup>
  <thead>
    <tr>
      <th>序号</th>
      <th>员工编号</th>
      <th>项目名称</th>
      <th>绩效</th>
      <th>操作</th>
    </tr> 
  </thead>
  <tbody id="user_tbody"></tbody>
</table>
<div id="pageInfo" style="text-align:right;padding-right: 30px"></div>
<script type="text/javascript">
element.render();

formOn('submit(sel)','/scoreServlet','json',function(data){
	
	var pageIndex = $("input[name='pageIndex']").val();
	var pageLimit = $("input[name='pageLimit']").val();
	setPageInfo("pageInfo",data.count,pageIndex,pageLimit,function(obj,first){
		$("input[name='pageIndex']").val(obj.curr);
		$("input[name='pageLimit']").val(obj.limit);
		if(! first){refresh()}
	})
	var html = "";
	 $.each(data.data,function(index,element){
		element.id = (index+1)+(pageIndex-1)*pageLimit;
		html+=getLaytpl("#tradd",element);
	})
	$("#user_tbody").html(html);
})
refresh();
function refresh(){
	$("input[value='查询']").click();
}
function openAdd(){
	openLayer("/web/page/score/add.jsp",refresh);
}
function del(employeeCode,projectCode){
	openConfirm(function(index){
		ajax("/scoreServlet",{employeeCode:employeeCode,projectCode:projectCode,action:"del"},"text",function(data){
			if(1==data){
				layer.msg("删除成功",{time:1000},refresh);
				
			}else{
				layer.msg("删除失败");
			}
		});
	},"确定是否删除该项目记录?")
}
function openUpd(employeeCode,projectCode){
	openLayer("/web/page/score/upd.jsp?employeeCode="+employeeCode+"&projectCode="+projectCode,refresh);
}
</script>
<script type="text/html" id="tradd">
<tr>
	<td>{{d.id}}</td>                    
	<td>{{d.employeeCode}}</td>
	<td>{{d.projectCode}}</td>
	<td>{{d.score}}</td>
	<td>
		<a href="javascript:del('{{d.employeeCode}}','{{d.projectCode}}');" class="layui-btn layui-btn-danger layui-btn-sm" >
			<i class="layui-icon layui-icon-delete"></i>
		</a>
		<input type="button" value="修改" class="layui-btn layui-btn-sm" onclick="openUpd('{{d.employeeCode}}','{{d.projectCode}}')" />
	</td>
</tr>
</script>
</body>
</html>