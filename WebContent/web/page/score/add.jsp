<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/web/header.jsp" %>
<title>项目添加</title>
</head>
<body>
<fieldset class="layui-elem-field">
	<legend>信息维护</legend>
	<div class="layui-field-box">
		<form action="" class="layui-form layui-form-pane" >
			<div class="layui-form-item">
				<label class="layui-form-label">员工部门</label>
    			<div class="layui-input-inline">
      				<select name="departmentCode" onchange="initSelect3()"></select>
    			</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">员工姓名</label>
    			<div class="layui-input-inline">
      				<select name="employeeCode"></select>
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">项目</label>
    			<div class="layui-input-inline">
      				<select name="projectCode" onchange="initSelect3()"></select>
    			</div>
			</div>
   	 		<div class="layui-form-item">
				<label class="layui-form-label">绩效</label>
    			<div class="layui-input-inline">
      				<input type="text" name="score" required  lay-verify="required" 
      				placeholder="请输入绩效" autocomplete="off" class="layui-input">
    			</div>
			</div>
			
   	 		<div class="layui-form-item">
    			<div class="layui-input-inline">
      				<input type="button" value="添加" class="layui-btn" lay-submit lay-filter="add" />
      				<input type="reset" value="重置" class="layui-btn layui-btn-primary" />
    			</div>
    			<input type="button" value="关闭" class="layui-btn" onclick="toClose()"/>
  			</div>
  			<input type="hidden" name="action" value="add" />
		</form>
	</div>
</fieldset>
<script type="text/javascript">
element.render();
initSelect1();
function initSelect1(){
	ajax("/departmentServlet",{action:"sel"},"json",function(data){
		var html = '';
		$.each(data,function(index,element){
			html += '<option value="'+this.code+'">'+this.name+'</option>';
		})
		$("select[name='departmentCode']").html(html);
	})
	ajax("/projectServlet",{action:"sel"},"json",function(data){
		var html = '';
		$.each(data,function(index,element){
			html += '<option value="'+this.code+'">'+this.name+'</option>';
		})
		$("select[name='projectCode']").html(html);
		
	})
	form.render();
}
/* function initSelect2(){
	console.log("-------")
	var departmentCode = $("select[name='departmentCode']").val();
	console.log(departmentCode)
	ajax("/employeeServlet",{departmentCode:departmentCode,action:"sel"},"json",function(data){
		
		var html = '';
		$.each(data,function(index,element){
			html += '<option value="'+this.code+'">'+this.name+'</option>';
		})
		$("select[name='employeeCode']").html(html);
		form.render();
	})
	
} */
function initSelect3(){
	console.log("++++++")
}
formOn('submit(add)','/scoreServlet','text',function(data){
	if(1==data){
		layer.msg("添加成功");
	}else if('E' == data){
		layer.msg("编号重复")			
	}else{
		layer.msg("添加失败");
	}
})
</script>
</body>
</html>