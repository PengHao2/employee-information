function toJsp(url){
	location.href=base.app+url;
}
function toJspReg(){
	toJsp('/web/reg.jsp');
}
function toJspLogin(){
	toJsp('/web/login.jsp')
}
var form = layui.form;
var $ = layui.jquery;
var layer = layui.layer;
var element = layui.element;
var laytpl = layui.laytpl;
var laypage = layui.laypage;
var upload = layui.upload;

function formOn(event,url,dataType,func){
	form.on(event,function(data){
		ajax(url,data.field,dataType,func)
	})
}
function ajax(url,field,dataType,func){
	$.ajax({
		type:"post",
		data:field,
		url:base.app+url,
		dataType:dataType,
		success:func
	})
}
function toClose(){
	var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭   
}
function openLayer(url,func){
	layer.open({
		type:2,//iframe
		area:['750px','550px'],
		maxmin:true,
		end:func,
		content:base.app + url
	})
}
function getLaytpl(sel,data){
	return laytpl($(sel).html()).render(data);
}
function setPageInfo(elem,count,curr,limit,jump){
	laypage.render({
		elem:elem,
		count:count,
		limits:[10,20,30,40,50,60,70,],
		layout:['count','prev','page','next','limit','refresh','skip'],
		curr:curr,
		limit:limit,
		jump:jump//当分页被操作时触发的回调方法(object当前分页的所有选项值,first是否首次,一般用于初始加载的判断)
	})
}
function openConfirm(func,title){
	layer.confirm(title?title:"是否进行此操作?",{icon:3,title:"提示"},func);
}
function renderUpload(elem,url,data,done){
	return upload.render({
		elem:'#'+elem,
		url:base.app+url,
		data:data,
		done:done,
	});
}







